package quizzer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Fact {
	private Section m_section;
	private String m_type;
	private List<Question> m_specialQuestions = new ArrayList<Question>();
	
	private HashMap<String, String> m_fields = new HashMap<String, String>();
	
	public Fact(Section s, String type) {
		m_section = s;
		m_type = type;
	}
	
	public Section getSection() { return m_section; }
	public String getType() { return m_type; }
	public Map<String, String> getFields() { return m_fields; }
	
	
	public void putField(String name, String value) {
		m_fields.put(name, value);
	}
	
	public void addSpecialQuestion(Question q) {
		m_specialQuestions.add(q);
	}
	
	public void generate(Quizzer q, List<QuestionBatch> questions) {
		QuestionBatch b = new QuestionBatch();
		
		Template t = q.getTemplate(m_type);
		t.generate(this, b);
		
		for (Question ques : m_specialQuestions) b.addQuestion(ques.shuffleAnswers());
	}
}
