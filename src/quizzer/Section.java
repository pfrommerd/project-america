package quizzer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

public class Section {
	private String m_name;
	private ArrayList<Fact> m_facts = new ArrayList<Fact>();

	public Section(String name) {
		m_name = name;
	}
	
	public String getName() { return m_name; }
	
	public void addFact(Fact f) {
		m_facts.add(f);
	}

	public List<Fact> getFacts() {
		return m_facts;
	}
	
	public List<QuestionBatch> generate(Quizzer q, List<Template> types) {
		ArrayList<QuestionBatch> questions = new ArrayList<QuestionBatch>();
		
		for (Fact f : m_facts) {
			inner:
			for (Template t : types) {
				if (f.getType().equals(t.getType())) {
					f.generate(q, questions);
					break inner;
				}
			}
		}

		Collections.shuffle(questions);
		
		return questions;
	}
	
	public List<QuestionBatch> generate(Quizzer q, List<Template> types, int n) {
		List<QuestionBatch> questions = generate(q, types);
		if (questions.size() < n) {
			throw new IllegalArgumentException("You asked for too many questions!");
		}
		return questions.subList(0, n);
	}
	
	public JsonValue asJson() {
		JsonObject o = new JsonObject();
		o.add("name", m_name);
		return o;
	}
}
