package quizzer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class QuestionFormat {	
	private String m_promptTemplate;
	private String m_answerTemplate;

	public QuestionFormat(String prompt, String answer) {
		m_promptTemplate = prompt;
		m_answerTemplate = answer;
	}
	
	public String getPromptTemplate() {
		return m_promptTemplate;
	}

	public String getAnswerTemplate() {
		return m_answerTemplate;
	}
	
	public Question generate(Fact f) {
		String question = s_replace(m_promptTemplate, f.getFields());
		Section s = f.getSection();
		
		ArrayList<String> answers = new ArrayList<String>();
		String answer = s_replace(m_answerTemplate, f.getFields());
		
		//First choice should be the correct one
		answers.add(answer);
		
		//Pick other random facts for other answers
		List<Fact> facts = new ArrayList<Fact>(s.getFacts());
		facts.remove(f);
		if (facts.size() < 3) {
			throw new IllegalArgumentException("Not enough facts of type " + f.getType() + " for section " + s.getName());
		}
		Collections.shuffle(facts);

		
		for (int i = 0; i < 3; i++) {
			String falseAnswer = s_replace(m_answerTemplate, facts.get(i).getFields());
			answers.add(falseAnswer);
		}
		
		Collections.shuffle(answers);
		
		return new Question(question, answers, answers.indexOf(answer));
	}
	
	
	public static String s_replace(String text, Map<String, String> fields) {
		Pattern patt = Pattern.compile("\\$\\{.*\\}");
		Matcher m = patt.matcher(text);
		StringBuffer sb = new StringBuffer(text.length());
		while (m.find()) {
			String match = m.group(0);

			String var = match.substring(2, match.length() - 1);
			if (fields.get(var) != null) {
				m.appendReplacement(sb, fields.get(var));
			}
		}
		m.appendTail(sb);
		return sb.toString();	
	}
	
	
}
