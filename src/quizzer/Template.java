package quizzer;

import java.util.List;

public class Template {
	private String m_type;
	private List<QuestionFormat> m_formats;
	
	public Template(String type, List<QuestionFormat> formats) {
		m_type = type;
		m_formats = formats;
	}

	public String getType() {
		return m_type;
	}

	public List<QuestionFormat> getFormats() {
		return m_formats;
	}

	public void generate(Fact f, QuestionBatch questions) {
		for (QuestionFormat format : m_formats) {
			questions.addQuestion(format.generate(f));
		}
	}
}
