package quizzer;

import java.util.ArrayList;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonValue;

// One batch per fact
public class QuestionBatch {
	private ArrayList<Question> m_questions = new ArrayList<Question>();
	
	public QuestionBatch() {
		
	}
	
	public Question getQuestion(int index) {
		return m_questions.get(index);
	}
	
	public void addQuestion(Question q) {
		m_questions.add(q);
	}
	
	public JsonValue asJson() {
		JsonArray array = new JsonArray();
		for (Question q : m_questions) array.add(q.asJson());
		return array;
	}
}
