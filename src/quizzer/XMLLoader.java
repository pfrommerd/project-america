package quizzer;

import java.util.ArrayList;
import java.util.Arrays;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;

public class XMLLoader {
	public static void s_load(String xml, Quizzer q) {
		Document doc = Jsoup.parse(xml, "", Parser.xmlParser());
		
		for (Element e : doc.select("template")) {
			s_parseTemplate(e, q);
		}
		
		for (Element e : doc.select("section")) {
			s_parseSection(e, q);
		}
	}
	
	public static void s_parseTemplate(Element t, Quizzer q) {
		String type = t.attr("type");
		ArrayList<QuestionFormat> formats = new ArrayList<QuestionFormat>();
		
		for (Element e : t.select("question")) {
			formats.add(s_parseFormat(e));
		}
		
		Template template = new Template(type, formats);
		q.addTemplate(template);
	}
	
	public static QuestionFormat s_parseFormat(Element format) {
		String prompt = format.attr("prompt");
		String answer = format.attr("answer");
		return new QuestionFormat(prompt, answer);
	}
	
	public static void s_parseSection(Element s, Quizzer q) {
		String name = s.attr("name");
		Section section = new Section(name);
		
		for (Element e : s.select("fact")) {
			section.addFact(s_parseFact(e, section));
		}
		
		q.addSection(section);
	}
	
	public static Fact s_parseFact(Element f, Section s) {
		String type = f.attr("type");
		
		Fact fact = new Fact(s, type);
		
		for (Element e : f.children()) {
			if (e.tagName().equals("question")) {
				String prompt = e.attr("prompt");
				
				ArrayList<String> answers = new ArrayList<String>();
				answers.add(e.attr("correct"));
				answers.addAll(Arrays.asList(e.attr("false").split(";")));
				fact.addSpecialQuestion(new Question(prompt, answers, 0));
			} else fact.putField(e.tagName(), e.text());
		}
		
		return fact;
	}
}
