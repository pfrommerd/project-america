package quizzer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collection;
import java.util.LinkedHashMap;

public class Quizzer {
	//Map of template type to template
	private LinkedHashMap<String, Template> m_templates = new LinkedHashMap<String, Template>();
	
	//List of sections
	private LinkedHashMap<String, Section> m_sections = new LinkedHashMap<String, Section>();
	
	public Quizzer() {
		
	}
	
	public Collection<Section> getSections() {
		return m_sections.values();
	}
	
	public Template getTemplate(String type) {
		return m_templates.get(type);
	}
	
	public Collection<Template> getTemplates() {
		return m_templates.values();
	}
	
	public Section getSection(String name) {
		return m_sections.get(name);
	}
	
	public void addTemplate(Template s) {
		m_templates.put(s.getType(), s);
	}
	
	public void addSection(Section s) {
		m_sections.put(s.getName(), s);
	}
	
	
	public void load(String xml) {
		XMLLoader.s_load(xml, this);
	}
	
	public void load(File f) {
		StringBuilder sb = new StringBuilder(2048);
	    try {
	        Reader r = new InputStreamReader(new FileInputStream(f), "UTF-8");
	        int c = 0;
	        while ((c = r.read()) != -1) {
	            sb.append((char) c);
	        }
	        r.close();
	    } catch (IOException e) {
	        throw new RuntimeException(e);
	    }
	    load(sb.toString());
	}
}
