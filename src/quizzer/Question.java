package quizzer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

public class Question {
	private String m_question;

	private ArrayList<String> m_answers = new ArrayList<String>();
	private int m_correctAnswer = 0;
	
	public Question(String question, ArrayList<String> answers, int correctAnswer) {
		m_question = question;
		m_answers = answers;
		m_correctAnswer = correctAnswer;
	}
	
	public String getQuestion() {
		return m_question;
	}
	
	public List<String> getAnswers() {
		return m_answers;
	}
	
	public int getCorrectAnswer() {
		return m_correctAnswer;
	}
	
	public int getPointValue() {
		return 10;
	}
	
	public Question shuffleAnswers() {
		ArrayList<String> answers = new ArrayList<String>(m_answers);
		Collections.shuffle(answers);
		int correct = answers.indexOf(m_answers.get(m_correctAnswer));
		return new Question(m_question, answers, correct);
	}
	
	public String toString() {
		String s = m_question;
		for (String a : m_answers) {
			s += "\n\t" + a;
		}
		return s;
	}
	
	public JsonValue asJson() {
		JsonObject o = new JsonObject();
		o.add("prompt", m_question);
		JsonArray answers = new JsonArray();
		for (String a : m_answers) answers.add(a);
		o.add("answers", answers);
		
		return o;
	}
}
