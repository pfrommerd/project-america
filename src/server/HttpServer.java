package server;

import java.io.IOException;
import java.util.Map;

import com.eclipsesource.json.JsonValue;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.Response.IStatus;


public class HttpServer extends NanoHTTPD {
	//The api is responsible for handling all the calls
	//which the server forwards it
	private API m_api = new API();
	
	public HttpServer(int port) {
		super(port);
	}
	
	@Override
	public void start() throws IOException {
		super.start();
	}
	
	@Override
	public Response serve(IHTTPSession session) {
		Method method = session.getMethod();
		
		String result = "404 Not found";
		
		if (method == Method.GET) {
			String uri = session.getUri();
			if (uri.startsWith("/api/"))	{
				JsonValue response = handleApiCall(uri.substring("/api/".length()), session.getParms());
				result = response.toString();
			}
		}
		
		Response r = new Response(result);
		
		String origin = session.getHeaders().get("origin");
		if (origin != null) r.addHeader("Access-Control-Allow-Origin", origin);
		return r;
	}
	
	public JsonValue handleApiCall(String call, Map<String, String> params) {
		switch(call) {
		case "connect": {
			return m_api.connect(params.get("name"));
		}
		
		case "disconnect": {
			long id = Long.parseLong(params.get("id"));
			return m_api.disconnect(id);
		}
		
		case "getSections": {
			long id = Long.parseLong(params.get("id"));
			return m_api.getSections(id);
		}
		
		case "getTypes": {
			long id = Long.parseLong(params.get("id"));
			return m_api.getTypes(id);			
		}
		
		case "startGame": {
			long id = Long.parseLong(params.get("id"));
			String name = params.get("name");
			boolean pub = Boolean.parseBoolean(params.get("public"));
			int numBatches = Integer.parseInt(params.get("numBatches"));
			String[] sections = params.get("sections").split(";");
			String[] types = params.get("types").split(";");
			return m_api.startGame(id, name, pub, numBatches, sections, types);
		}
		
		case "leaveGame": {
			long id = Long.parseLong(params.get("id"));
			return m_api.leaveGame(id);
		}
		
		case "getGames": {
			long id = Long.parseLong(params.get("id"));
			return m_api.getGames(id);
		}
		
		case "joinGame": {
			long id = Long.parseLong(params.get("id"));
			long gameID = Long.parseLong(params.get("gameID"));
			return m_api.joinGame(id, gameID);
		}
		
		case "events": {
			long id = Long.parseLong(params.get("id"));
			return m_api.events(id);
		}
		
		case "submit": {
			long id = Long.parseLong(params.get("id"));
			int questionIndex = Integer.parseInt(params.get("question"));
			int answerIndex = Integer.parseInt(params.get("answer"));
			return m_api.submit(id, questionIndex, answerIndex);
		}
		
		case "advanceQuestionBatch": {
			long id = Long.parseLong(params.get("id"));
			return m_api.advanceQuestionBatch(id);
		}
		
		default: return API.s_createError("Unknown API call: " + call);
		}
	}
	


	public static void main(String[] args) throws IOException {
		String portName = System.getenv("PORT");
		int port = 8081;
		if (portName != null) port = Integer.parseInt(portName);
		
		
		HttpServer server = new HttpServer(port);
		server.start();

		System.out.println("Server started on port " + port);

		Object o = new Object();
		try {
			synchronized(o) {
				o.wait();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("Server started");
		
	}
}
