package server;

import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import quizzer.Question;
import quizzer.QuestionBatch;
import quizzer.Quizzer;
import quizzer.Section;
import quizzer.Template;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

public class Game {
	private static int s_idCounter = 0;
	
	private Quizzer m_quizzer;
	
	private long m_id = 0;
	private String m_name;
	private boolean m_public;
	//
	private int m_batchesPerSection = 0;
	private List<Section> m_sections = null;
	private List<Template> m_types = null;
	
	//
	private CopyOnWriteArrayList<Client> m_clients = new CopyOnWriteArrayList<Client>();
	
	private ConcurrentHashMap<Client, Integer> m_scores = new ConcurrentHashMap<Client, Integer>();
	
	//The questionbatch which the client is on
	private ConcurrentHashMap<Client, QuestionBatch> m_currentQuestions = new ConcurrentHashMap<Client, QuestionBatch>();
	//The queue of questionbatches
	private ConcurrentHashMap<Client, List<QuestionBatch>> m_questions = new ConcurrentHashMap<Client, List<QuestionBatch>>();
	
	//The sections which the clients are on
	private ConcurrentHashMap<Client, Section> m_currentSections = new ConcurrentHashMap<Client, Section>();
	
	public Game(Quizzer q, Client owner, String name, boolean pub, int batchesPerSection, List<Section> sections, List<Template> types) {
		m_quizzer = q;
		m_id = ++s_idCounter;
		m_name = name;
		m_public = pub;
		
		m_batchesPerSection = batchesPerSection;
		m_sections = sections;
		m_types = types;
		
		m_clients.add(owner);
		owner.setGame(this);
	}
	
	public String getName() { return m_name; }
	public long getID() { return m_id; }
	public boolean isPublic() { return m_public; }
	public int numPeople() { return m_clients.size(); }
	
	public void clientJoined(Client c) {
		m_clients.add(c);
		c.setGame(this);
		
		m_scores.put(c, 0);
	}
	
	public void clientLeft(Client c) {
		m_clients.remove(c);
		c.setGame(null);;
	}
	
	// Called by Clinet when it receives a submission
	// returns the response to the client (right or wrong, point value)
	public JsonValue clientSubmitted(Client c, int questionNum, int choice) {
		QuestionBatch batch = m_currentQuestions.get(c);
		
		Question q = batch.getQuestion(questionNum);
		
		boolean correct = q.getCorrectAnswer() == choice;
		int score = m_scores.get(c);
		
		// If incorrect deduct half the point value
		int scoreChange = correct ? q.getPointValue() : -(q.getPointValue() >> 1);
		m_scores.put(c, score + scoreChange);
			
		broadcastScores();
		
		//Correct
		JsonObject o = new JsonObject();
		o.add("question", questionNum);
		o.add("correct", correct);
		o.add("score", m_scores.get(c));
		o.add("scoreChange", scoreChange);
		return o;
	}
	
	public void advanceQuestionBatch(Client c) {
		Section s = m_currentSections.get(c);
		s = m_currentSections.get(c);
		
		List<QuestionBatch> questions = m_questions.get(c);
		
		if (s == null || questions == null || questions.size() == 0) {
			// Advance the section
			if (s == null) {
				s = m_sections.get(0);
			} else {
				int idx = m_sections.indexOf(s) + 1;
				if (idx >= m_sections.size()) {
					// We're done
					showEndScreen(c);
					broadcastScores();
					return;
				}
				s = m_sections.get(idx);
				m_currentSections.put(c, s);
				
				JsonObject section = new JsonObject();
				section.add("name", s.getName());
				c.addEvent("section", section);
			}
	
			//Generate new question list
			if (m_batchesPerSection > 0) questions = s.generate(m_quizzer, m_types, m_batchesPerSection);
			else questions = s.generate(m_quizzer, m_types);
			
			m_questions.put(c, questions);
		}
		
		QuestionBatch n = questions.get(0);
		
		m_currentQuestions.put(c, n);
		questions.remove(0);
		
		// Send an event
		c.addEvent("questionBatch", n.asJson());
	}
	
	public void showEndScreen(Client c) {
		JsonObject done = new JsonObject();
		c.addEvent("done", done);
		broadcastScores();
	}
	
	public void broadcastScores() {
		JsonObject scores = new JsonObject();
		for (Entry<Client, Integer> entry : m_scores.entrySet()) {
			String name = entry.getKey().getName();
			int score = entry.getValue();
			scores.add(name, score);
		}
		
		for (Client c : m_clients) {
			c.addEvent("scores", scores);
		}
	}
	
	public JsonValue asJson() {
		JsonObject game = new JsonObject();
		game.add("name", m_name);
		game.add("gameID", m_id);

		game.add("isPublic", m_public);
		game.add("questionsPerSection", m_batchesPerSection);
		
		JsonArray sections = new JsonArray();
		for (Section s : m_sections) sections.add(s.getName());
		
		JsonArray types = new JsonArray();
		for (Template t : m_types) types.add(t.getType());
		
		game.add("sections", sections);
		game.add("types", types);
		return game;
	}
}
