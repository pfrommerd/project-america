package server;

import server.API.APIException;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

public class Client {
	private static long s_idCounter = 0;

	//
	private JsonArray m_events = new JsonArray();
	
	private long m_id;
	private String m_name;
	private Game m_game;
	
	public Client(String name) {
		this(++s_idCounter, name);
	}
	
	public Client(long id, String name) {
		m_id = id;
		m_name = name;
	}
	
	public long getID() { return m_id; }
	public String getName() { return m_name; }
	
	public Game getGame() { return m_game; }
	public void setGame(Game g) { m_game = g; }
	
	public void addEvent(String name, JsonValue value) {
		JsonObject event = new JsonObject();
		event.add("event", name);
		event.add("data", value);
		m_events.add(event);
	}
	
	public JsonValue submit(int questionIndex, int answerIndex) {
		if (m_game == null) throw new APIException("Client " + m_id + " is not part of any game!");
		return m_game.clientSubmitted(this, questionIndex, answerIndex);
	}
	
	public void nextQuestionBatch() {
		if (m_game == null) throw new APIException("Client " + m_id + " is not part of any game!");
		m_game.advanceQuestionBatch(this);
	}
	
	public JsonValue events() {
		JsonArray events = m_events;
		m_events = new JsonArray();
		return events;
	}
	
	@Override
	public int hashCode() {
		return (int) m_id;
	}
}
