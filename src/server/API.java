package server;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import quizzer.Quizzer;
import quizzer.Section;
import quizzer.Template;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

public class API {
	public static class APIException extends RuntimeException {
		private static final long serialVersionUID = -5024062382762490378L;
		public APIException(String text) {
			super(text);
		}
	}
	
	private Quizzer m_quizzer = new Quizzer();
	
	private ConcurrentHashMap<Long, Client> m_clientMap = new ConcurrentHashMap<Long, Client>();
	private ConcurrentHashMap<Long, Game> m_gameMap = new ConcurrentHashMap<Long, Game>();
	
	public API() {
		m_quizzer.load(new File("resources/questions.am"));
	}
	
	public JsonValue connect(String name) {
		try {
			Client c = new Client(name);
			m_clientMap.put(c.getID(), c);

			JsonObject response = new JsonObject();
			response.add("id", c.getID());

			return s_createResponse("connected", response);
		} catch (Exception e) {
			return s_createError(e.getMessage());
		}
	}
	

	public JsonValue disconnect(long id) {
		try {
			Client c = m_clientMap.get(id);
			if (c == null) throw new APIException("No client with id: " + id);
			
			Game g = c.getGame();
			if (g != null) g.clientLeft(c);
			m_clientMap.remove(c);
			
			return s_createResponse("disconnected", new JsonObject());
		} catch (Exception e) {
			return s_createError(e.getMessage());
		}
	}

	public JsonValue startGame(long id, String name, boolean pub, int numBatches, String[] sectionNames, String[] typeNames) {
		try {
			Client owner = m_clientMap.get(id);
			if (owner == null) throw new APIException("No client with id: " + id);
			
			List<Section> sections = new ArrayList<Section>();
			for (String s : sectionNames) {
				Section section = m_quizzer.getSection(s);
				if (section == null) throw new APIException("No section with name: " + s);
				sections.add(section);
			}
			List<Template> types = new ArrayList<Template>();
			for (String s : typeNames) {
				Template type = m_quizzer.getTemplate(s);
				if (type == null) throw new APIException("No section with name: " + s);
				types.add(type);
			}
			
			Game game = new Game(m_quizzer, owner, name, pub, numBatches, sections, types);

			m_gameMap.put(game.getID(), game);

			return s_createResponse("gameStarted", game.asJson());
		} catch (Exception e) {
			return s_createError(e.getMessage());
		}
	}

	public JsonValue joinGame(long id, long gameID) {
		try {
			Client c = m_clientMap.get(id);
			if (c == null) throw new APIException("No client with id: " + id);
			
			Game game = m_gameMap.get(gameID);
			if (game == null) throw new APIException("No game with id: " + gameID);

			game.clientJoined(c);
			
			return s_createResponse("joined", new JsonObject());
			
		} catch (Exception e) {
			return s_createError(e.getMessage());
		}
	}
	
	public JsonValue leaveGame(long id) {
		try {
			Client client = m_clientMap.get(id);
			if (client == null) throw new APIException("No client with id: " + id);

			Game game = client.getGame();
			if (game == null) throw new APIException("Not currently part of a game!");

			game.clientLeft(client);
			if (game.numPeople() < 1) {
				// Destroy the game
				m_gameMap.remove(game.getID());
			}

			return s_createResponse("left", new JsonObject());
		} catch (Exception e) {
			return s_createError(e.getMessage());
		}
	}


	public JsonValue getTypes(long id) {
		try {
			JsonArray array = new JsonArray();
			for (Template t : m_quizzer.getTemplates()) {
				array.add(t.getType());
			}
			
			return s_createResponse("types", array);
		} catch (Exception e) {
			return s_createError(e.getMessage());
		}
	}

	public JsonValue getSections(long id) {
		try {
			JsonArray array = new JsonArray();
			for (Section s : m_quizzer.getSections()) {
				array.add(s.asJson());
			}
			return s_createResponse("sections", array);
		} catch (Exception e) {
			return s_createError(e.getMessage());
		}
	}
	
	public JsonValue getGames(long id) {
		try {
			JsonArray array = new JsonArray();
			for (Game g : m_gameMap.values()) {
				if (g.isPublic()) array.add(g.asJson());
			}

			return s_createResponse("games", array);
		} catch (Exception e) {
			return s_createError(e.getMessage());
		}
	}

	public JsonValue submit(long id, int questionIndex, int answerIndex) {
		try {
			Client client = m_clientMap.get(id);
			if (client == null) throw new APIException("No client with id: " + id);

			return s_createResponse("submitted", client.submit(questionIndex, answerIndex));
		} catch (Exception e) {
			return s_createError(e.getMessage());
		}
	}
	
	@SuppressWarnings("deprecation")
	public JsonValue advanceQuestionBatch(long id) {
		try {
			Client client = m_clientMap.get(id);
			if (client == null) throw new APIException("No client with id: " + id);
			
			client.nextQuestionBatch();
			
			return s_createResponse("questionBatchAdvanced", JsonValue.valueOf("Check events!"));
		} catch (Exception e) {
			return s_createError(e.getMessage());
		}
	}

	public JsonValue events(long id) {
		Client client = m_clientMap.get(id);
		if (client == null) throw new APIException("No client with id: " + id);		
		return s_createResponse("events", client.events());
	}
	
	public static JsonValue s_createError(String msg) {
		JsonObject o = new JsonObject();
		o.add("type", "error");
		o.add("data", msg);
		return o;
	}
	
	public static JsonValue s_createResponse(String type, JsonValue response) {
		JsonObject o = new JsonObject();
		o.add("type", type);
		o.add("data", response);
		return o;		
	}
}
